﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkStudio.Memo.Plugins.SearchForm
{
    public interface ISearchForm
    {
        /// <summary>
        /// 検索画面を表示します
        /// </summary>
        void ShowForm();

    } //interface
}
