﻿
using System.Text;


namespace WorkStudio.Memo.Plugins.Interfaces
{
    interface IEncodingPlugin
    {
        /// <summary>
        /// エンコーディング(文字コード)
        /// </summary>
        Encoding TextEncoding { get; set; }
    }
}
