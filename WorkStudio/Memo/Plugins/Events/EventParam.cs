﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace WorkStudio.Memo.Plugins.Events
{
    /// <summary>
    /// イベント用パラメーター
    /// </summary>
    public class EventParam
    {
        /// <summary>
        /// true:イベントを途中で中止にする
        /// </summary>
        public bool Cancel = false;

    } //class
}
